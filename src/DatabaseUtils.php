<?php

namespace Rockschtar\WordPress\DatabaseUtils;

use Rockschtar\WordPress\DatabaseUtils\Exceptions\DatabaseException;

class DatabaseUtils {

    /**
     * @param String $table
     * @param array $data
     * @param array $format
     * @throws DatabaseException
     * @return int
     */
    public static function insert(String $table, array $data = [], array $format = []): int {

        global $wpdb;

        $result = $wpdb->insert($table, $data, $format);

        if($result === false) {
            throw new DatabaseException($wpdb->last_error);
        }

        return $wpdb->insert_id;
    }

    /**
     * @param String $table
     * @param array $data
     * @param array $where
     * @param array|null $format
     * @param array|null $where_format
     * @return bool
     * @throws DatabaseException
     */
    public static function update(String $table, array $data, array $where, array $format = null, array $where_format = null): bool {
        global $wpdb;

        $result = $wpdb->update($table, $data, $where, $format, $where_format);

        if($result === false) {
            throw new DatabaseException($wpdb->last_error);
        }

        return true;
    }

    /**
     * @param string $table
     * @param array $where
     * @param array|null $where_format
     * @return int
     * @throws DatabaseException
     */
    public static function delete(string $table, array $where, array $where_format = null): int {
        global $wpdb;

        $result = $wpdb->delete($table, $where, $where_format);

        if($result === false) {
            throw new DatabaseException($wpdb->last_error);
        }

        return (int)$result;
    }

    /**
     * @param string $query
     * @param int $x
     * @param int $y
     * @return null|string
     * @throws DatabaseException
     */
    public static function getVar(string $query, $x = 0, $y = 0): ?string {
        global $wpdb;

        $result = $wpdb->get_var($query, $x, $y);

        if($result === null) {
            throw new DatabaseException($wpdb->last_error);
        }

        return $result;
    }

    /**
     * @param string $query
     * @param string $output
     * @return array|null
     * @throws DatabaseException
     */
    public static function getResults(string $query, string $output = 'OBJECT'): ?array {
        global $wpdb;
        $wpdb->last_error = '';
        $result = $wpdb->get_results($query, $output);
        if(!empty($wpdb->last_error)) {
            throw new DatabaseException($wpdb->last_error);
        }

        return $result;
    }

    /**
     * @param string $query
     * @param array $prepare
     * @return false|int
     * @throws DatabaseException
     */
    public static function query(string $query, array $prepare = []) {
        global $wpdb;

        $prepared_query = \count($prepare) === 0 ? $query : $wpdb->prepare($query, $prepare);

        $result = $wpdb->query($prepared_query);

        if(!empty($wpdb->last_error)) {
            throw new DatabaseException($wpdb->last_error);
        }

        return $result;

    }

    /**
     * @param string $query
     * @param array $prepare
     * @param string $output (Optional) The required return type. One of OBJECT, ARRAY_A, or ARRAY_N, which correspond to an stdClass object, an associative array, or a numeric array, respectively. Default value: OBJECT
     * @param int $y (Optional) Row to return. Indexed from 0.
     * @return array|null|object
     * @throws DatabaseException
     */
    public static function getRow(string $query, array $prepare = [], string $output = OBJECT, $y = 0) {
        global $wpdb;

        $prepared_query = \count($prepare) === 0 ? $query : $wpdb->prepare($query, $prepare);

        $result = $wpdb->get_row($prepared_query, $output, $y);

        if(!empty($wpdb->last_error)) {
            throw new DatabaseException($wpdb->last_error);
        }

        return $result;
    }

}